import { Injectable, EventEmitter } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingService {
  ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10),
  ];
  ingredientAdded = new EventEmitter<Ingredient>();

  constructor() { }

  getIngrediants() {
    return this.ingredients.slice();
  }

  addIngrediant(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.ingredientAdded.emit(ingredient);
  }

  addIngrediants(ingredients: Ingredient []) {
    this.ingredients.push(...ingredients);
  }
}
