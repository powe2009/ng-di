import { Injectable, EventEmitter } from '@angular/core';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  recipes: Recipe[] = [
    new Recipe(
      'Fish \'n Chips',
      'Moist and crispy',
      'https://i.ibb.co/9YKC0rs/fish-chips.jpg',
       [
         new Ingredient ('cod', 1),
         new Ingredient ('chips', 20)
       ]
    ),
    new Recipe(
      'Burger',
      'Big and juicy',
      'https://i.ibb.co/LhFK6n8/1371606262739.jpg',
       [
         new Ingredient ('beef', 1),
         new Ingredient ('fries', 20)
       ]
    )
  ];
  recipeSelected = new EventEmitter<Recipe>();

  constructor() { }

  getRecipes() {
    return this.recipes.slice();
  }
}
